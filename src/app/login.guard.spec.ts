import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { Router } from '@angular/router'
import { Location } from '@angular/common'
import { HttpClientTestingModule } from '@angular/common/http/testing'

import { LoginGuard } from './login.guard'
import { SharedService } from './shared.service'
import { routes } from './app-routing.module'
import { AppComponent } from './app.component'

describe('LoginGuard', () => {
  let loginGuard: LoginGuard
  let sharedService: SharedService
  let router: Router
  let location: Location
  let fixture

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        RouterTestingModule.withRoutes(routes),
        HttpClientTestingModule
      ],
      providers: [LoginGuard, SharedService]
    }).compileComponents()

    sharedService = TestBed.inject(SharedService)
    loginGuard = TestBed.inject(LoginGuard)
    router = TestBed.inject(Router)
    location = TestBed.inject(Location)
    fixture = TestBed.createComponent(AppComponent)

    router.initialNavigation()
  })

  it('not be able to navigate to /dashboard when user is not logged in', async () => {
    await router.navigate(['/dashboard'])
    await expect(location.path()).toBe('/')
  })

  it('should navigate to /dashboard for a logged in user', async () => {
    sharedService.email = 'JUST_VALUE'
    await router.navigate(['/dashboard'])
    await expect(location.path()).toMatch('/dashboard')
  })
})
