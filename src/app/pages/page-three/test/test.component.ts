import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter
} from '@angular/core'

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit, OnChanges {
  previousPrice: string
  parentPrice: string

  @Input() price: string
  @Output() parentPriceEvent = new EventEmitter<string>()

  constructor() {}

  updateParentPrice(price: string) {
    this.parentPriceEvent.emit(price)
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (!changes.price.firstChange) {
      this.previousPrice = changes.price.previousValue
    }
  }
}
