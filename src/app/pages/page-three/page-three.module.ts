import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'
import { MatListModule } from '@angular/material/list'
import { CurrencyDirective } from '../../currency.directive'

import { PageThreeRoutingModule } from './page-three-routing.module'
import { PageThreeComponent } from './page-three.component'
import { TestComponent } from './test/test.component'

@NgModule({
  declarations: [PageThreeComponent, CurrencyDirective, TestComponent],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    PageThreeRoutingModule
  ]
})
export class PageThreeModule {}
