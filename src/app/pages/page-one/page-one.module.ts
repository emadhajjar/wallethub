import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'

import { PageOneRoutingModule } from './page-one-routing.module'
import { PageOneComponent } from './page-one.component'
import { RatingComponent } from '../../components/rating/rating.component'

@NgModule({
  declarations: [PageOneComponent, RatingComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    PageOneRoutingModule
  ]
})
export class PageOneModule {}
