import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './dashboard.component'

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: 'page-one',
        pathMatch: 'full'
      },
      {
        path: 'page-one',
        loadChildren: () =>
          import('../pages/page-one/page-one.module').then(m => m.PageOneModule)
      },
      {
        path: 'page-two',
        loadChildren: () =>
          import('../pages/page-two/page-two.module').then(m => m.PageTwoModule)
      },
      {
        path: 'page-three',
        loadChildren: () =>
          import('../pages/page-three/page-three.module').then(
            m => m.PageThreeModule
          )
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
