import {
  Directive,
  Input,
  OnInit,
  OnChanges,
  HostListener,
  ElementRef,
  forwardRef
} from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

@Directive({
  selector: '[appCurrency]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CurrencyDirective),
      multi: true
    }
  ]
})
export class CurrencyDirective
  implements OnInit, OnChanges, ControlValueAccessor {
  private htmlInputElement: HTMLInputElement

  @Input() prefix = '$'

  constructor(private elementRef: ElementRef) {}

  onChangeCallback = (value: any) => {}
  onTouchedCallback = () => {}

  writeValue(value: any) {
    this.buildValue(value)
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn
  }

  @HostListener('keyup', ['$event'])
  handleKeyup(event: KeyboardEvent) {
    if (['Backspace', 'Delete'].includes(event.key)) {
      this.formatValue()
    }

    event.preventDefault()
  }

  @HostListener('keypress', ['$event'])
  handleKeypress(event: KeyboardEvent) {
    if (
      [
        'Backspace',
        'Tab',
        'End',
        'Home',
        'ArrowLeft',
        'ArrowRight',
        'Delete'
      ].includes(event.key)
    ) {
      return
    } else if (event.key.match(/\d/) !== null) {
      const { value, selectionStart, selectionEnd } = this.htmlInputElement

      const next = [
        value.slice(0, selectionStart),
        event.key,
        value.slice(selectionEnd)
      ].join('')
      const formated = this.buildValue(next)
      const newPos = selectionStart + 1 + (formated.length - next.length)
      this.htmlInputElement.setSelectionRange(newPos, newPos)
    }

    event.preventDefault()
  }

  @HostListener('cut', ['$event'])
  handleCut(event: ClipboardEvent) {
    event.preventDefault()
  }

  @HostListener('paste', ['$event'])
  handlePaste(event: ClipboardEvent) {
    event.preventDefault()
  }

  buildValue(value: string) {
    const formated = `${this.prefix}${String(value)
      .replace(/\D/g, '')
      .replace(/\d(?=(\d{3})+$)/g, '$&,')}`

    this.onChangeCallback(formated)
    this.htmlInputElement.value = formated
    return formated
  }

  formatValue() {
    const { value } = this.htmlInputElement
    this.buildValue(value)
  }

  ngOnInit() {
    this.htmlInputElement = this.elementRef.nativeElement
    this.formatValue()
  }

  ngOnChanges() {
    if (this.htmlInputElement) this.formatValue()
  }
}
