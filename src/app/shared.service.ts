import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  percentDone: number
  error: boolean
  email: string

  constructor() {
    this.percentDone = 100
    this.error = false
    this.email = ''
  }
}
